# UE LIFPF INF2030L Programmation Fonctionnelle

## Semestre 2023 Printemps

[Configuration pour la salle de TP](CONFIGURATION.md)

| jour  | heure         | type     | supports / remarques                                                                        |
| ----- | ------------- | -------- | ------------------------------------------------------------------------------------------- |
| 16/01 | 8h            | CM       | [Diapositives](cm/lifpf-cm1.pdf)                                                            |
|       | 9h45          | TD       | [Sujet](td/lifpf-td1-enonce.pdf)                                                            |
| 23/01 | 8h            | CM       | [Diapositives](cm/lifpf-cm2.pdf), [Script démos](cm/cm2-demo.md)                            |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp1.md), [corrigé](tp/tp1.ml) <br> Groupe de TP, horaire et salle sur [tomuss]   |
| 30/01 | 8h            | TD + QCM | [Sujet](td/lifpf-td2-enonce.pdf)                                                            |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp2.md), [corrigé](tp/tp2.ml) <br> Groupe de TP, horaire et salle sur [tomuss]   |
| 20/02 | 8h            | CM       | [Diapositives](cm/lifpf-cm3.pdf)                                                            |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp3.md) <br> Groupe de TP, horaire et salle sur [tomuss]                         |
| 27/02 | 8h            | TD + QCM | [Sujet](td/lifpf-td3-enonce.pdf) <br> Attention groupes **A et E** en salle **Nautibus C2** |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp4.md) <br> Groupe de TP, horaire et salle sur [tomuss]                         |

###### Évaluation

- 5 QCMs en TD: 60 %
- ECA: 40%

Selon l'avancement du cours, il est possible qu'il y ait un TP noté lors de la dernière séance. Dans ce cas, il comptera dans les 60% des QCMs

[tomuss]: https://tomuss.univ-lyon1.fr
